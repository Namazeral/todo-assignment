// GENERATED BY THE COMMAND ABOVE; DO NOT EDIT
// This file was generated by swaggo/swag

package docs

import (
	"bytes"
	"encoding/json"
	"strings"

	"github.com/alecthomas/template"
	"github.com/swaggo/swag"
)

var doc = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{.Description}}",
        "title": "{{.Title}}",
        "contact": {
            "name": "Namaz Eral",
            "email": "namazeral@gmail.com"
        },
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/todos": {
            "get": {
                "description": "Get all of the tasks",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Tasks"
                ],
                "summary": "Get all of the tasks",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/structs.Swag_task"
                            }
                        }
                    }
                }
            },
            "put": {
                "description": "Update an existing task with the input payload",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Tasks"
                ],
                "summary": "Update an existing task",
                "parameters": [
                    {
                        "description": "Update Task",
                        "name": "task",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/structs.Swag_task"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/structs.Swag_task_update"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/structs.Handler_error"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/structs.Handler_error"
                        }
                    }
                }
            },
            "post": {
                "description": "Create a new task with the input payload",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Tasks"
                ],
                "summary": "Create a new task",
                "parameters": [
                    {
                        "description": "Create Task",
                        "name": "task",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/structs.Swag_task"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/structs.Swag_task"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/structs.Handler_error"
                        }
                    }
                }
            }
        },
        "/todos/{id}": {
            "get": {
                "description": "Get a specific task with id as an input in the parameter",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Tasks"
                ],
                "summary": "Get a specific task",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/structs.Swag_task"
                        }
                    }
                }
            },
            "delete": {
                "description": "Get a specific task with id as an input in the parameter",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Tasks"
                ],
                "summary": "Get a specific task",
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/structs.Handler_error"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/structs.Handler_error"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "structs.Handler_error": {
            "type": "object",
            "properties": {
                "error_code": {
                    "type": "integer",
                    "example": 400
                },
                "error_desc": {
                    "type": "string",
                    "example": "Bad Request, Got an Error"
                },
                "error_msg": {
                    "type": "string",
                    "example": "Failed to run"
                }
            }
        },
        "structs.Swag_assigned": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string",
                    "example": "albert"
                }
            }
        },
        "structs.Swag_task": {
            "type": "object",
            "properties": {
                "assigned_to": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/structs.Swag_assigned"
                    }
                },
                "deadline": {
                    "type": "string",
                    "example": "2019-11-09T21:21:46Z"
                },
                "description": {
                    "type": "string",
                    "example": "New feature"
                },
                "task_id": {
                    "type": "integer",
                    "example": 1
                },
                "type_task": {
                    "type": "string",
                    "example": "Tech"
                }
            }
        },
        "structs.Swag_task_update": {
            "type": "object",
            "properties": {
                "fail_assigned": {
                    "type": "array",
                    "items": {
                        "type": "integer"
                    },
                    "example": [
                        2
                    ]
                },
                "result": {
                    "type": "string",
                    "example": "successfully updated task 1"
                },
                "success_assigned": {
                    "type": "array",
                    "items": {
                        "type": "integer"
                    },
                    "example": [
                        6
                    ]
                }
            }
        }
    }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Schemes     []string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = swaggerInfo{
	Version:     "1.0",
	Host:        "localhost:3000",
	BasePath:    "",
	Schemes:     []string{},
	Title:       "Todo Application",
	Description: "An API for add task and assign the task",
}

type s struct{}

func (s *s) ReadDoc() string {
	sInfo := SwaggerInfo
	sInfo.Description = strings.Replace(sInfo.Description, "\n", "\\n", -1)

	t, err := template.New("swagger_info").Funcs(template.FuncMap{
		"marshal": func(v interface{}) string {
			a, _ := json.Marshal(v)
			return string(a)
		},
	}).Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, sInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}
