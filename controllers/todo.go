package controllers

import (
	"net/http"
	"todo-assignment/structs"

	"github.com/gin-gonic/gin"
)

type DataResult struct {
	structs.Task
	Assigned_to []structs.Assigned
}

func Resp_serializer(task structs.Task, assigned_to []structs.Assigned) map[string]interface{} {
	res_data := map[string]interface{}{
		"Task_id":     task.Task_id,
		"Description": task.Description,
		"Type_task":   task.Type_task,
		"Deadline":    task.Deadline,
		"Assigned_to": assigned_to,
	}
	return res_data
}

func Input_task(input DataResult) structs.Task {
	new_task := structs.Task{}
	new_task.Description = input.Description
	new_task.Type_task = input.Type_task
	new_task.Deadline = input.Deadline
	return new_task
}

func Input_assign(input structs.Assigned) structs.Assigned {
	new_assign := structs.Assigned{}
	new_assign.Name = input.Name
	new_assign.Task_id = input.Task_id
	return new_assign
}

// GetTask godoc
// @Summary Get a specific task
// @Description Get a specific task with id as an input in the parameter
// @Tags Tasks
// @Accept json
// @Produce json
// @Success 200 {object} structs.Swag_task
// @Router /todos/{id} [get]
func (idb *InDB) GetTask(c *gin.Context) {
	var (
		task        structs.Task
		assigned_to []structs.Assigned
		result      gin.H
	)
	id := c.Param("id")
	err := idb.DB.Where("task_id = ?", id).First(&task).Error
	if err != nil {
		result = gin.H{
			"result": err.Error(),
			"count":  0,
		}
	} else {
		var data = DataResult{}
		data.Task = task
		err := idb.DB.Where("task_id = ?", id).Find(&assigned_to).Error
		if err != nil {
			data.Assigned_to = nil
		} else {
			data.Assigned_to = assigned_to
		}
		result = gin.H{
			"result": data,
			"count":  1,
		}
	}
	c.JSON(http.StatusOK, result)
}

// GetTasks godoc
// @Summary Get all of the tasks
// @Description Get all of the tasks
// @Tags Tasks
// @Accept json
// @Produce json
// @Success 200 {array} structs.Swag_task
// @Router /todos [get]
func (idb *InDB) GetTasks(c *gin.Context) {
	var (
		tasks  []structs.Task
		res    []DataResult
		result gin.H
	)

	idb.DB.Find(&tasks)
	if len(tasks) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		for _, task := range tasks {
			var data = DataResult{}
			var assigned_to = []structs.Assigned{}
			data.Task = task
			err := idb.DB.Where("task_id = ?", task.Task_id).Find(&assigned_to).Error
			if err != nil {
				data.Assigned_to = nil
			} else {
				data.Assigned_to = assigned_to
			}
			res = append(res, data)
		}
		result = gin.H{
			"result": res,
			"count":  len(tasks),
		}
	}
	c.JSON(http.StatusOK, result)
}

// CreateTask godoc
// @Summary Create a new task
// @Description Create a new task with the input payload
// @Tags Tasks
// @Accept json
// @Produce json
// @Param task body structs.Swag_task true "Create Task"
// @Success 200 {object} structs.Swag_task
// @Failure 400 {object} structs.Handler_error
// @Router /todos [post]
func (idb *InDB) CreateTask(c *gin.Context) {
	var (
		handler structs.Handler_error
		input   DataResult
		result  gin.H
	)
	// Validate input
	if err := c.ShouldBindJSON(&input); err != nil {
		handler.Error_code = http.StatusBadRequest
		handler.Error_desc = err.Error()
		handler.Error_msg = "Input is not valid"
		c.JSON(http.StatusBadRequest, gin.H{"result": handler})
		return
	}
	task := Input_task(input)
	idb.DB.Create(&task)
	new_assigned := []structs.Assigned{}
	for _, i := range input.Assigned_to {
		i.Task_id = task.Task_id
		idb.DB.Create(&i)
		new_assigned = append(new_assigned, i)
	}
	res_data := Resp_serializer(task, new_assigned)
	result = gin.H{
		"result": res_data,
	}
	c.JSON(http.StatusOK, result)
}

// CreateTask godoc
// @Summary Update an existing task
// @Description Update an existing task with the input payload
// @Tags Tasks
// @Accept json
// @Produce json
// @Param task body structs.Swag_task true "Update Task"
// @Success 200 {object} structs.Swag_task_update
// @Failure 400 {object} structs.Handler_error
// @Failure 404 {object} structs.Handler_error
// @Router /todos [put]
func (idb *InDB) UpdateTask(c *gin.Context) {
	var (
		handler     structs.Handler_error
		input       DataResult
		task        structs.Task
		assigned_to []structs.Assigned
		result      gin.H
	)
	// Validate input
	if err := c.ShouldBindJSON(&input); err != nil {
		handler.Error_code = http.StatusBadRequest
		handler.Error_desc = err.Error()
		handler.Error_msg = "Input is not valid"
		c.JSON(http.StatusBadRequest, gin.H{"result": handler})
		return
	}
	err := idb.DB.Where("Task_id = ?", input.Task_id).First(&task).Error
	if err != nil {
		handler.Error_code = http.StatusBadRequest
		handler.Error_desc = err.Error()
		handler.Error_msg = "Task not found"
		result = gin.H{
			"result": "data not found",
		}
		c.JSON(http.StatusNotFound, result)
		return
	}
	newTask := Input_task(input)
	err = idb.DB.Model(&task).Updates(newTask).Error
	if err != nil {
		handler.Error_code = http.StatusBadRequest
		handler.Error_desc = err.Error()
		handler.Error_msg = "Failed Update the task"
		result = gin.H{
			"result": handler,
		}
		c.JSON(http.StatusBadRequest, result)
		return
	}
	success_assigned := []uint64{}
	fail_assigned := []uint64{}
	if len(input.Assigned_to) > 0 {
		// delete every assign
		err = idb.DB.Where("Task_id = ?", input.Task_id).Find(&assigned_to).Error
		if err != nil {
			handler.Error_code = http.StatusBadRequest
			handler.Error_desc = err.Error()
			handler.Error_msg = "Failed Assigning the task"
			result = gin.H{
				"result": handler,
			}
			c.JSON(http.StatusBadRequest, result)
			return
		}
		for _, i := range assigned_to {
			idb.DB.Delete(&i)
		}

		// input new assign
		for _, i := range input.Assigned_to {

			var assigned = structs.Assigned{}
			err := idb.DB.Where("Task_id = ?", input.Task_id).Where("Assigned_id = ?", i.Assigned_id).First(&assigned).Error
			if err != nil {
				i.Task_id = task.Task_id
				idb.DB.Create(&i)
				success_assigned = append(success_assigned, i.Assigned_id)
				continue
			}
			new_assigned := Input_assign(i)
			err = idb.DB.Model(&assigned).Updates(new_assigned).Error
			if err != nil {
				fail_assigned = append(fail_assigned, i.Assigned_id)
			} else {
				success_assigned = append(success_assigned, i.Assigned_id)
			}
		}
	}
	result = gin.H{
		"result":           "successfully updated task " + string(rune(input.Task_id)),
		"fail_assigned":    fail_assigned,
		"success_assigned": success_assigned,
	}
	c.JSON(http.StatusOK, result)
}

// GetTask godoc
// @Summary Get a specific task
// @Description Get a specific task with id as an input in the parameter
// @Tags Tasks
// @Accept json
// @Produce json
// @Success 200
// @Failure 400 {object} structs.Handler_error
// @Failure 404 {object} structs.Handler_error
// @Router /todos/{id} [delete]
func (idb *InDB) DeleteTask(c *gin.Context) {
	var (
		handler     structs.Handler_error
		task        structs.Task
		assigned_to []structs.Assigned
		result      gin.H
	)
	task_id := c.Param("id")
	err := idb.DB.Where("Task_id = ?", task_id).First(&task).Error
	if err != nil {
		handler.Error_code = http.StatusBadRequest
		handler.Error_desc = err.Error()
		handler.Error_msg = "Task not found"
		result = gin.H{
			"result": handler,
		}
		c.JSON(http.StatusNotFound, result)
		return
	}

	err = idb.DB.Delete(&task).Error
	if err != nil {
		handler.Error_code = http.StatusBadRequest
		handler.Error_desc = err.Error()
		handler.Error_msg = "Failed deleting the task"
		result = gin.H{
			"result": "delete failed",
		}
		c.JSON(http.StatusBadRequest, result)
		return
	}
	err = idb.DB.Where("Task_id = ?", task_id).Find(&assigned_to).Error
	if err != nil {
		handler.Error_code = http.StatusBadRequest
		handler.Error_desc = err.Error()
		handler.Error_msg = "Failed delete the assign"
		result = gin.H{
			"result": "delete failed",
		}
		c.JSON(http.StatusBadRequest, result)
		return
	}
	for _, i := range assigned_to {
		idb.DB.Delete(&i)
	}
	result = gin.H{
		"result": "successfully deleted task " + task_id,
	}
	c.JSON(http.StatusOK, result)
}
