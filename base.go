package main

import (
	"todo-assignment/config"
	"todo-assignment/controllers"

	_ "todo-assignment/docs"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title Todo Application
// @version 1.0
// @description An API for add task and assign the task
// @contact.name Namaz Eral
// @contact.email namazeral@gmail.com
// @host localhost:3000
func main() {
	db := config.DBInit()
	inDB := &controllers.InDB{DB: db}

	router := gin.Default()

	router.GET("/todos/:id", inDB.GetTask)
	router.GET("/todos", inDB.GetTasks)
	router.POST("/todos", inDB.CreateTask)
	router.PUT("/todos", inDB.UpdateTask)
	router.DELETE("/todos/:id", inDB.DeleteTask)

	//Swagger
	url := ginSwagger.URL("http://localhost:3000/swagger/doc.json") // The url pointing to API definition
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	router.Run(":3000")
}
