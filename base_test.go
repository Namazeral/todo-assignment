package main

import (
	"testing"
	"todo-assignment/controllers"
	"todo-assignment/structs"

	"github.com/stretchr/testify/assert"
)

func Test_resp(t *testing.T) {
	var (
		task structs.Task
	)
	task.Description = "a"
	task.Type_task = "Tech"
	task.Deadline = "24-5-2022"
	task.Task_id = '1'

	assigned := []structs.Assigned{
		{Name: "Albert"},
		{Name: "Jeremy"},
	}
	expected_result := map[string]interface{}{
		"Task_id":     task.Task_id,
		"Description": task.Description,
		"Type_task":   task.Type_task,
		"Deadline":    task.Deadline,
		"Assigned_to": assigned,
	}
	assert.Equal(t, controllers.Resp_serializer(task, assigned), expected_result, "result and the expected result is not the same") // or what value you need it to be
}

func Test_input_task(t *testing.T) {
	var (
		input controllers.DataResult
	)
	input.Description = "a"
	input.Type_task = "Tech"
	input.Deadline = "24-5-2022"

	expected_result := structs.Task{
		Description: input.Description,
		Type_task:   input.Type_task,
		Deadline:    input.Deadline,
	}
	assert.Equal(t, controllers.Input_task(input), expected_result, "result and the expected result is not the same") // or what value you need it to be
}

func Test_input_assign(t *testing.T) {
	var (
		input structs.Assigned
	)

	input.Task_id = '1'
	input.Name = "Albert"
	expected_result := structs.Assigned{
		Task_id: input.Task_id,
		Name:    input.Name,
	}
	assert.Equal(t, controllers.Input_assign(input), expected_result, "result and the expected result is not the same") // or what value you need it to be
}
