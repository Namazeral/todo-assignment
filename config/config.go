package config

import (
	"todo-assignment/structs"

	"github.com/jinzhu/gorm"
)

// UPDATE DB HERE
var db_username string = "namaz"
var db_password string = "Jetpack1234"
var db_address string = "localhost"
var db_port string = "3306"
var db_name string = "task_management"

func DBInit() *gorm.DB {
	db, err := gorm.Open("mysql", db_username+":"+db_password+"@tcp("+db_address+":"+db_port+")/"+db_name+"?charset=utf8mb4&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(structs.Task{})
	db.AutoMigrate(structs.Assigned{})
	return db
}
