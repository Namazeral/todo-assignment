package structs

type Assigned struct {
	Assigned_id uint64 `gorm:"primary_key" example:"1"`
	Name        string `example:"albert"`
	Task_id     uint64 `gorm:"ForeignKey:Task_id;References:Task_id" example:"1"`
}

type Task struct {
	Task_id     uint64 `gorm:"primary_key" example:"1"`
	Description string `example:"New feature"`
	Type_task   string `example:"Tech"`
	Deadline    string `example:"2019-11-09T21:21:46Z"`
}
