package structs

type Handler_error struct {
	Error_code int    `example:"400"`
	Error_desc string `example:"Bad Request, Got an Error"`
	Error_msg  string `example:"Failed to run"`
}
