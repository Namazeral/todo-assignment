package structs

type Swag_assigned struct {
	Name string `example:"albert"`
}

type Swag_task struct {
	Task_id     uint64 `gorm:"primary_key" example:"1"`
	Description string `example:"New feature"`
	Type_task   string `example:"Tech"`
	Deadline    string `example:"2019-11-09T21:21:46Z"`
	Assigned_to []Swag_assigned
}

type Swag_task_update struct {
	Result           string   `example:"successfully updated task 1"`
	Fail_assigned    []uint64 `example:"2"`
	Success_assigned []uint64 `example:"6"`
}
